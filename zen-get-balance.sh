#!/bin/bash

DIR="$(dirname "$0")"
. "$DIR/config.cfg"

APIKEY="ca1a3e9b7021eb7ec35d703978aad2873849c87e"

COIN="ZEN"
ZEN_API="https://securenodes.na.zensystem.io/api/nodes"

ZEN_EXPLORER_OLD="http://explorer.zenmine.pro" # dejo de funcionar
ZEN_EXPLORER="https://explorer.zensystem.io"
ZEN_PRICE_EUR=$(curl --silent https://api.coinmarketcap.com/v1/ticker/zencash/?convert=EUR | jq '.[0].price_eur' | bc)
ZEN_PRICE_EUR=$(curl --silent https://api.coinmarketcap.com/v1/ticker/zencash/?convert=EUR | jq '.[0].price_eur' | bc)
ZEN_BALANCE_OLD=$(cat /tmp/zen_balance.txt 2> /dev/null || echo "${#MN[@]} * 42" | bc)
ZEN_LAST_PAYMENT=$(curl -s "${ZEN_API}/my/payments?key=${APIKEY}&page=1&rows=30" | jq -r '.rows[] | select(.status == "paid").enddate' | head -n 1)

TOTAL_M=0
ZEN_MN_ERROR=0

DAYS_AGO="$(( (`date +%s` - `date -d $ZEN_LAST_PAYMENT +%s`) / 86400 )) days ago"

# Get Balance
for i in "${MN[@]}"
do
  BALANCE=$(curl --silent "$ZEN_EXPLORER/insight-api-zen/addr/$i/?noTxList=1" | jq '.balance')
  TOTAL_M=$(echo "${TOTAL_M} + ${BALANCE}" | bc)
done

TOTAL=$TOTAL_M

for i in "${A[@]}"
do
  BALANCE=$(curl --silent "$ZEN_EXPLORER/insight-api-zen/addr/$i/?noTxList=1" | jq '.balance')
  TOTAL=$(echo "${TOTAL} + ${BALANCE}" | bc)
done

TOTAL_F=$(echo "$TOTAL" | bc | xargs printf "%'.0f\n")
TOTAL_W=$(echo "${ZEN_PRICE_EUR} * ${TOTAL}" | bc | xargs printf "%'.0f\n")

## Double check status
for i in "${MNL[@]}"
do

  NODE_INFO=$(curl -s "${ZEN_API}/${i}/detail?key=${APIKEY}")

  NODE_STATUS=$(echo "$NODE_INFO" | jq -r '.status')
  NODE_EXCEPTION=$(echo "$NODE_INFO" | jq -r '.exceptions')
  NODE_FQDN=$(echo "$NODE_INFO" | jq -r '.fqdn')

  if [ "$NODE_STATUS" != "up" ]; then
    /usr/local/bin/slackr -c warning "[$COIN]💸 ERROR MN DOWN - $NODE_FQDN"
    ZEN_MN_ERROR=$(($ZEN_MN_ERROR + 1))
  fi

  if [ "$NODE_EXCEPTION" != "[]" ]; then
    /usr/local/bin/slackr -c warning "[$COIN]💸 ERROR MN EXCEPTION - $NODE_FQDN"
    ZEN_MN_ERROR=$(($ZEN_MN_ERROR + 1))
  fi

done

if [ "$ZEN_MN_ERROR" -eq "0" ]; then
  FOOTER="All Masternodes are healthy"
else
  FOOTER="${ZEN_MN_ERROR} MN have an error"
fi

TITLE="[$COIN] Total $TOTAL_F - 🏦 $TOTAL_W €"

# Earn
MINED=$(echo "${TOTAL_M} - ${ZEN_BALANCE_OLD}" | bc | xargs printf "%'.2f")
SOME_EARN=$(echo $MINED'>'0 | bc -l)

if [ "$SOME_EARN" -gt "0" ]; then
  EARNED=$(echo "${ZEN_PRICE_EUR} * ${MINED}" | bc | xargs printf "%0.2f\n")
  /usr/local/bin/slackr -f "$FOOTER" -t "$TITLE" -c good "💰$MINED Earned - $EARNED €"
else
  /usr/local/bin/slackr -f "$FOOTER" -t "$TITLE" -c warning "💸 $DAYS_AGO"
fi

echo $TOTAL_M > /tmp/zen_balance.txt
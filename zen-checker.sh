#!/bin/bash

COIN="ZEN"
DAEMON="zend"
RESTART="/usr/bin/zend"
CLI="/usr/bin/zen-cli"

lines=$(pgrep $DAEMON | wc -l)

if [ ! "$lines" -gt "0" ]; then
  $RESTART
  /usr/local/bin/slackr -c danger \[$COIN\] Restart $(hostname)
  exit 1
fi

status=$(/usr/local/bin/pm2 jlist | jq -r '.[0] | select(.name == "securenodetracker").pm2_env.status')

if [ "$status" != "online" ]; then
  /usr/local/bin/slackr -c danger \[$COIN\] PM2 Masternode Down $(hostname)
  exit 1
fi